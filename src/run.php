<?php

require_once __DIR__ . '/../vendor/autoload.php';


use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Interactions\WebDriverActions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;

// setup selenium with chrome and install adblocker
$host = 'http://localhost:4444';
$options = new ChromeOptions();
$options->addExtensions(['https://www.crx4chrome.com/go.php?p=31931&s=1&l=https%3A%2F%2Fclients2.googleusercontent.com%2Fcrx%2Fblobs%2FAcy1k0YD-f4h4-R0u-HzModk1GBd2hnTmM4qTTItW2IIhp_aM64Oik5J8-DoUElU6hkwzMa7K8zL9iIYlDLQlWvHajQBRGQXdF2cM399IExRI6dPEgkihADGUprlxu8JftL8uW2k9Jt95WgYYpjqC88%2Fextension_1_41_8_0.crx']);
$capabilities = DesiredCapabilities::chrome();
$capabilities->setCapability(ChromeOptions::CAPABILITY, $options);
$driver = RemoteWebDriver::create($host, $capabilities);

//load question:cssId as array to click on it (because it's faster than search it first)
$cssArray = JSON_DECODE(file_get_contents(__DIR__ . '/question_cssIds_map.json'), true);

//navigate to game and maximize chrome
$driver->get('https://online.seterra.com/en/vgp/3007');
$driver->manage()->window()->maximize();

// game loop
$totalTime = time();    //start global timer
    for ($j = 0; $j < 100; $j++) {  //play 100 rounds
        $startTime = microtime("float");    //start round timer
        $driver->findElement(WebDriverBy::id('cmdRestart'))->click();   //click the restart button
        for ($i = 0; $i < 46; $i++) {   //loop for every 46 countries
            $question = $driver->findElement(WebDriverBy::id('currQuestion'));  //get question text
            $land = substr($question->getText(), 11); //cut off the first 11 chars to get land only
            $cssId = $cssArray[$land]; //get cssId entry from the loaded array
            $cssElementToClick = $driver->findElement(WebDriverBy::id($cssId)); //find cssId in game

            $action = new WebDriverActions($driver);    //create new action
            clickAction($cssId, $action, $cssElementToClick); //click on element
        }
        echo 'Run completed in ' . (microtime("float") - $startTime) . ' at ' . (time() - $totalTime) /60 . PHP_EOL;

        sleep(2);
        try {
            $driver->findElement(WebDriverBy::id('btnComplete'))->click();  //click ok button if finisch
        } catch (Throwable) {
            echo "Error found. Game not count!" . PHP_EOL;  //if not clickable one ore more clicks missed
        }
    }


$driver->quit();



function clickAction(string $cssId, WebDriverActions $action, RemoteWebElement $clickArea): void
{
    // correct clicking for some special landscapes
    switch ($cssId) {
        case 'AREA_KOSOVO':
            $action->moveToElement($clickArea)->moveByOffset(-10, 0)->click()->perform();
            break;
        case 'AREA_CROATIA':
            $action->moveToElement($clickArea)->moveByOffset(0, -40)->click()->perform();
            break;
        case 'AREA_NORWAY':
            $action->moveToElement($clickArea)->moveByOffset(-150, 100)->click()->perform();
            break;
        case 'AREA_ITALY':
            $action->moveToElement($clickArea)->moveByOffset(0, -15)->click()->perform();
            break;
        case 'AREA_PORTUGAL':
            $action->moveToElement($clickArea)->moveByOffset(-20, 0)->click()->perform();
            break;
        case 'AREA_ALBANIA':
            $action->moveToElement($clickArea)->moveByOffset(5, 0)->click()->perform();
            break;
        default:
            $action->click($clickArea)->perform();
    }
}
