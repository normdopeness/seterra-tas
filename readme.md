## Selenium TAS
### written in PHP


Thanks to [selenium](https://github.com/SeleniumHQ/docker-selenium) for the docker-compose file, i use.


How to use?

Edit the Folder where runs are saved (*Default = D:/video*) in the
docker-compose-v3-video.yml file

On default, Chrome is used.

install dependencies
```php composer.phar install```

run
``docker-compose -f docker-compose-v3-video.yml up``

then run the script
```php ./src/run.php```

